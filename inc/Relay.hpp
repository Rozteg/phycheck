#ifndef _RELAY_H
#define _RELAY_H

#include <GpioChip.hpp>

class Relay
{

private:
    GpioChip* gpioChip;
    int gpioPin;

public:
    Relay(GpioChip* chip, int pin);
    ~Relay();
    int Close();
    int Open();
};

#endif // _RELAY_H 