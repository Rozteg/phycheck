#ifndef _CONNECTION_LINE_H
#define _CONNECTION_LINE_H

#include <Serial.hpp>
#include <GpioChip.hpp>

class ConnectionLine : public Serial
{

private:
    GpioChip* gpioChip;
    int gpioPin;

public:
    ConnectionLine(std::string portName, speed_t portSpeed, GpioChip* chip, int pin);
    ~ConnectionLine();
    int Write(std::string message);
    int Open();
};

#endif // _CONNECTION_LINE_H 