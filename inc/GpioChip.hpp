#ifndef _GPIO_CHIP_H
#define _GPIO_CHIP_H

#include <cstdio>
#include <iostream>
#include <iterator>
#include <vector>
#include <string>
//#include <cstdlib>

#include <fcntl.h>   /* File Control Definitions           */
#include <unistd.h>  /* UNIX Standard Definitions 	   */ 
#include <errno.h>   /* ERROR Number Definitions           */
#include <linux/gpio.h>
#include <sys/ioctl.h>

/* directions */
// GPIOHANDLE_REQUEST_OUTPUT
// GPIOHANDLE_REQUEST_INPUT

struct pinfd {
    pinfd () : fd(0), direction(0), outval(-1) {}
    int fd;
    int direction;
    int outval;
};

class GpioChip {

private:
    int fd;
    std::string name;
    std::vector<struct pinfd> pinFdsVec;
    struct gpiochip_info cinfo;
    struct gpioline_info linfo;

public:
    GpioChip(std::string chipName);
    ~GpioChip();
    int Open();
    int OpenPin(int pin, int direction);
    void ClosePin(int pin);
    int Read(int pin);
    int Write(int pin, int value);
    int GetLines();
    void Close();
    void ClosePinFd(int pin);
};

#endif // _GPIO_CHIP_H 