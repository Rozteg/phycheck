#include <sstream>
//#include <thread> // doesn't work in cross-compile
#include <pthread.h>

#include "Serial.hpp"
#include "GpioChip.hpp"
#include "ConnectionLine.hpp"
#include "Relay.hpp"

using namespace std;

struct relayThreadStruct {
    GpioChip* gpio;
    int dkPin;
    int grPin;
    Relay* relay;
    Relay* heat1;
    Relay* heat2;
};

void* relay_thread(void* in_data)
{
    // IF GERKON OTKL RELE OTKL
    // IF DK 3 SEK RELE OTKL 3.6 SEK

    struct relayThreadStruct* data;
    data = (struct relayThreadStruct *) in_data;

    int prevDk = 0;
    int prevGr = 0;

    int dkCounter = 0;

    int alarmCounter = 0;

    while(1)
    {
	usleep(100000); // 1/10 of a second
	int grState = data->gpio->Read(data->grPin);

	if (grState != prevGr)
	{
	    if (grState)
		data->relay->Close();
	    else
		data->relay->Open();
	    prevGr = grState;
	}

	int dkState = data->gpio->Read(data->dkPin);
	dkCounter += dkState;
	
	if (dkState == 0)
	{
	    if ((dkCounter > 20) && (dkCounter < 50))
	    {
		alarmCounter = 37; // 3.7 seconds
		data->relay->Open();
	    }

	    // TURN OFF HEATING
	    if ((dkCounter > 1) && (dkCounter <= 20))
	    {
		data->heat1->Open();
		data->heat2->Open();
	    }

	    // turn on heat1
	    if ((dkCounter > 50) && (dkCounter <= 100))
		data->heat1->Close();

	    // turn on heat2
	    if ((dkCounter > 100) && (dkCounter <= 150))
		data->heat2->Close();

	    dkCounter = 0;
	}

	if (alarmCounter > 0)
	{
	    alarmCounter -= 1;
	    if (alarmCounter == 0)
		data->relay->Close();
	}
    }

    pthread_exit(NULL);
}

int main(int argc, char **argv)
{

    /* Init */

    GpioChip GPIO3("/dev/gpiochip2");
    GPIO3.Open();
    GPIO3.OpenPin(6, GPIOHANDLE_REQUEST_OUTPUT);   // RE_DE 1
    GPIO3.OpenPin(12, GPIOHANDLE_REQUEST_OUTPUT);
    GPIO3.OpenPin(13, GPIOHANDLE_REQUEST_OUTPUT);
    GPIO3.OpenPin(14, GPIOHANDLE_REQUEST_OUTPUT);
    GPIO3.OpenPin(15, GPIOHANDLE_REQUEST_OUTPUT);
    GPIO3.OpenPin(16, GPIOHANDLE_REQUEST_OUTPUT);
    GPIO3.OpenPin(18, GPIOHANDLE_REQUEST_OUTPUT); // RE_DE 2
    GPIO3.OpenPin(19, GPIOHANDLE_REQUEST_OUTPUT); // LED
    GPIO3.OpenPin(20, GPIOHANDLE_REQUEST_OUTPUT);
    GPIO3.OpenPin(24, GPIOHANDLE_REQUEST_OUTPUT); // RELAY 1
    GPIO3.OpenPin(27, GPIOHANDLE_REQUEST_OUTPUT); // 12V R1
    GPIO3.OpenPin(28, GPIOHANDLE_REQUEST_OUTPUT); // 12V R2

    GPIO3.OpenPin(26, GPIOHANDLE_REQUEST_INPUT);  // GERKON
    GPIO3.OpenPin(23, GPIOHANDLE_REQUEST_INPUT);  // DK

    ConnectionLine L1("/dev/ttymxc1", B115200, &GPIO3, 6);
    L1.Open();

    ConnectionLine L2("/dev/ttymxc2", B115200, &GPIO3, 18);
    L2.Open();

    Relay R1(&GPIO3, 24);
    R1.Close();

    Relay TWEV1(&GPIO3, 27);
    TWEV1.Open();

    Relay TWEV2(&GPIO3, 28);
    TWEV2.Open();

    // using pthread!
    struct relayThreadStruct data;
    data.gpio = &GPIO3;
    data.dkPin = 23;
    data.grPin = 26;
    data.relay = &R1;
    data.heat1 = &TWEV1;
    data.heat2 = &TWEV2;

    pthread_t Rthread;
    if(pthread_create(&Rthread, NULL, relay_thread, (void *) &data))
	cout << "Unable to create thread" << "\r\n";
    else
	pthread_detach(Rthread);
    /* thread init eend  */

    /* actual check */

    vector<string> messages;
    messages.reserve(10);

    while (1)
    {

	usleep(100000);
	vector<string> inc1 = L1.ReadMessages();
	vector<string> inc2 = L2.ReadMessages();
	
	messages.insert(messages.end(), inc1.begin(), inc1.end());
	messages.insert(messages.end(), inc2.begin(), inc2.end());
	
/*	stringstream ss2(incoming);
	while (getline(ss2, token, '\n'))
	{
	    if (token != "")
	    messages.push_back(token);
	}
*/

	/* process messages */

	for (int i = 0; i < messages.size(); i++)
	{
	    cout << messages[i] << "\n";
	
	    if (messages[i] == "ECHO")
	    {
		L1.Write("ECHO");
		L2.Write("ECHO");
	    }
	
	    if (messages[i] == "RELAY1")
		R1.Close();
	
	    if (messages[i] == "RELAY0")
		R1.Open();

	    if (messages[i] == "12V11")
		TWEV1.Close();
	
	    if (messages[i] == "12V10")
		TWEV1.Open();

	    if (messages[i] == "12V21")
		TWEV2.Close();
	
	    if (messages[i] == "12V20")
		TWEV2.Open();
	}

	messages.clear();
    }

    R1.Open();
    TWEV1.Open();
    TWEV2.Open();
    L1.Close();
    L2.Close();
    GPIO3.Close();

    return 0;
}
