#include <Relay.hpp>

using namespace std;

Relay::Relay(GpioChip* chip, int pin)
{
    gpioChip = chip;
    gpioPin = pin;
}

Relay::~Relay()
{
}

int Relay::Close()
{
    cout << "Relay closed!\n";
    return (*gpioChip).Write(gpioPin, 1);
}


int Relay::Open()
{
    cout << "Relay opened!\n";
    return (*gpioChip).Write(gpioPin, 0);
}


