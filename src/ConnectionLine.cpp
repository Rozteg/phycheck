#include <ConnectionLine.hpp>

using namespace std;

ConnectionLine::ConnectionLine(string portName, speed_t portSpeed, GpioChip* chip, int pin)
    : Serial(portName, portSpeed)
{
    gpioChip = chip;
    gpioPin  = pin;
}

ConnectionLine::~ConnectionLine() {}

int ConnectionLine::Open()
{
    int res;
    res = Serial::Open();
    (*gpioChip).Write(gpioPin, 0);

    return res;
}

int ConnectionLine::Write(string message)
{
    int bytes;
    (*gpioChip).Write(gpioPin, 1);
    bytes = Serial::Write(message);
    tcdrain(fd);
    (*gpioChip).Write(gpioPin, 0);

    return bytes;
}
