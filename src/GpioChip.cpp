#include <GpioChip.hpp>

using namespace std;

GpioChip::GpioChip(string chipName)
{
    name = chipName;
    fd = 0;
}

GpioChip::~GpioChip()
{
    Close();
}

int GpioChip::Open()
{
    Close();

    int ret = 0;

    fd = open(name.c_str(), 0);

    if(fd == -1) /* Error Checking */
    {
	cerr << "Error! in Opening " << name  << " \n\r";
	return fd;
    }
    else
	cout << name << " Opened Successfully \n\r";

    struct gpiochip_info ChipInfo;
    ret = ioctl(fd, GPIO_GET_CHIPINFO_IOCTL, &ChipInfo);
    cinfo = ChipInfo;
//    printf("GPIO chip: %s, \"%s\", %u GPIO lines\n", cinfo.name, cinfo.label, cinfo.lines);
    cout << cinfo.name << " " << cinfo.label << " " << cinfo.lines << "\n";
    pinFdsVec.resize(cinfo.lines);

    struct gpioline_info LineInfo;
    ret = ioctl(fd, GPIO_GET_LINEINFO_IOCTL, &LineInfo);
    linfo = LineInfo;
//    printf("line %d: %s\n", linfo.line_offset, linfo.name);
//    cout << linfo.line_offset << " " << linfo.name << "\n"; 

    return fd;
}


int GpioChip::OpenPin(int pin, int direction)
{
    if (fd < 1)
	return -1;

    if ((pin > cinfo.lines - 1) || (pin < 0))
	return -2;

    if (!(direction == GPIOHANDLE_REQUEST_INPUT || 
	direction == GPIOHANDLE_REQUEST_OUTPUT))
	return -3;

    if (pinFdsVec[pin].fd > 0)
	return 0;

    struct gpiohandle_request req;
    req.lineoffsets[0] = pin;
    req.lines = 1;
    req.flags = direction;
    int lhfd = ioctl(fd, GPIO_GET_LINEHANDLE_IOCTL, &req);
    if (req.fd > 0)
    {
	pinFdsVec[pin].fd = req.fd;
	pinFdsVec[pin].direction = direction;
    }

    return req.fd;
}

void GpioChip::ClosePin(int pin)
{
    if (pinFdsVec[pin].fd > 0)
    {
	close(pinFdsVec[pin].fd);
	pinFdsVec[pin].direction = 0;
	pinFdsVec[pin].outval = -1;
    }
}

int GpioChip::Read(int pin)
{
    int ret = 0;

    if ((pin > cinfo.lines - 1) || (pin < 0))
	return -1;

    if (pinFdsVec[pin].fd < 1)
	return -2;

    if (pinFdsVec[pin].direction != GPIOHANDLE_REQUEST_INPUT)
	return -3;

    struct gpiohandle_data data;
    ret = ioctl(pinFdsVec[pin].fd, GPIOHANDLE_GET_LINE_VALUES_IOCTL, &data);
//    printf("line %d is in %d\n", pin, data.values[0]);

    return ((int) data.values[0]);
}

int GpioChip::Write(int pin, int value)
{
    int ret = 0;

    if ((pin > cinfo.lines - 1) || (pin < 0))
	return -1;

    if (pinFdsVec[pin].fd < 1)
	return -2;

    if (value < 0 || value > 1)
	return -3;

    struct gpiohandle_data data;
    data.values[0] = value;
    ret = ioctl(pinFdsVec[pin].fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data);
    pinFdsVec[pin].outval = value;
    //printf("line %d is in %d\n", pin, (int) data.values[0]);

    return ret;
}

int GpioChip::GetLines()
{
    return cinfo.lines;
}

void GpioChip::Close()
{
    if (fd > 0)
    {
        close(fd);
        fd = 0;

        for (int i = 0; i < pinFdsVec.size(); i++)
	    ClosePin(i);
    }
}








